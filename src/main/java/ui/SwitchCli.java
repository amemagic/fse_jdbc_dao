package ui;

import dataaccess.MySqlCourseRepository;
import dataaccess.MySqlStudentRepository;

import java.sql.SQLException;
import java.util.Scanner;

public class SwitchCli implements Cli {

    Scanner scan;
    Cli studentCli;
    Cli courseCli;

    public SwitchCli(){
        this.scan = new Scanner(System.in);
    }

    {
        try {
            studentCli = new StudentCli(new MySqlStudentRepository());
            courseCli = new CourseCli(new MySqlCourseRepository());
        } catch (SQLException e) {
            System.out.println("Fehler bei Datenbankverbindung.");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void start() {
        String input = "-";
        while(!input.equals("x"))
        {
            showMenue();
            input = scan.nextLine();

            switch(input) {
                case "1":
                    studentCli.start();
                    break;
                case "2":
                    courseCli.start();
                    break;
                case "x":
                    System.out.println("Auf Wiedersehen");
                    break;
                default:
                    inputError();
                    break;
            }
        }
        scan.close();
    }

    @Override
    public void showMenue() {
        System.out.println("------------------- KURSSYSTEM -------------------");
        System.out.println("(1) STUDENTENMANAGEMENT \t (2) KURSMANAGEMENT");
        System.out.println("(x) ENDE");
    }

    private void inputError(){
        System.out.println("Bitte nur die Zahlen der Menüauswahl eingeben");
    }
}
