package ui;

import dataaccess.DatabaseException;
import dataaccess.MyStudentRepository;
import domain.Course;
import domain.InvalidValueException;
import domain.Student;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class StudentCli implements Cli {

    Scanner scan;
    MyStudentRepository repo;

    public StudentCli(MyStudentRepository repo)
    {
        this.scan = new Scanner(System.in);
        this.repo = repo;
    }

    @Override
    public void start() {
        String input = "-";
        while(!input.equals("x"))
        {
            showMenue();
            input = scan.nextLine();
            switch(input) {
                case "1":
                    addStudent();
                    break;
                case "2":
                    showAllStudents();
                    break;
                case "3":
                    showStudentDetails();
                    break;
                case "4":
                    // updateCourseDetails();
                    break;
                case "5":
                    // deleteCourse();
                    break;
                case "6":
                    // courseSearch();
                    break;
                case "x":
                    break;
                default:
                    inputError();
                    break;
            }
        }
    }

    private void showStudentDetails() {
        Long id;
        System.out.println("Bitte geben Sie die Studenten-ID ein:");
        id = scan.nextLong();
        try {
            Optional<Student> student = repo.getById(id);
            if(student.isEmpty()){
                System.out.println("Kein Student mit der id gefunden");
            } else {
                System.out.println(student.get());
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Anzeigen des Studenten: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten: ");
        }
    }

    private void showAllStudents() {

        List<Student> list = null;
        try {
            list = repo.getAll();
            if(list.size()>0)
            {
                for (Student student:list) {
                    System.out.println(student);
                }
            } else {
                System.out.println("Studentenliste leer!");
            }
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler bei Anzeigen aller Studenten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten: " + exception.getMessage());
        }


    }

    private void addStudent() {
        String firstname, lastname, email, major;
        Date birthDate, admissionDate;

        try
        {
            System.out.println("Bitte alle Studentendaten angeben:");
            System.out.println("Vorname: ");
            firstname = scan.nextLine();
            if(firstname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            System.out.println("Nachname: ");
            lastname = scan.nextLine();
            if(lastname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            System.out.println("Geburtsdatum (YYYY-MM-DD): ");
            birthDate = Date.valueOf(scan.nextLine());
            System.out.println("E-Mail:");
            email = scan.nextLine();
            System.out.println("Studiengang:");
            major = scan.nextLine();
            System.out.println("Anfangsdatum (YYYY-MM-DD): ");
            admissionDate = Date.valueOf(scan.nextLine());

            Optional<Student> optionalStudent = repo.insert(
                    new Student(firstname, lastname, birthDate, email, major, admissionDate)
            );

            if(optionalStudent.isPresent())
            {
                System.out.println("Student angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException)
        {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException)
        {
            System.out.println("Studentendaten nicht korrekt angegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException)
        {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception)
        {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }

    }

    @Override
    public void showMenue() {
        System.out.println("------------------- STUDENTENMANAGEMENT -------------------");
        System.out.println("(1) Student eingeben \t (2) Alle Studenten anzeigen \t" + "(3) Student anzeigen");
        System.out.println("(4) Studentendetails ändern \t (5) Student löschen \t" + "(6) Studentensuche");
        System.out.println("(x) ENDE");
    }

    private void inputError(){
        System.out.println("Bitte nur die Zahlen der Menüauswahl eingeben");
    }

}
