package domain;

import java.sql.Date;
import java.util.Set;

public class Student extends BaseEntity {

    private String firstName;
    private String lastName;
    private Date birthDate;
    private String email;
    private String major;
    private Date admissionDate;

    private Set<Course> courses;

    public Student(Long id, String firstName, String lastName, Date birthDate, String email, String major, Date admissionDate) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.major = major;
        this.admissionDate = admissionDate;
    }

    public Student(String firstName, String lastName, Date birthDate, String email, String major, Date admissionDate) {
        super(null);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
        this.major = major;
        this.admissionDate = admissionDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + this.getId() + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", email='" + email + '\'' +
                ", major='" + major + '\'' +
                ", admissionDate=" + admissionDate +
                '}';
    }

    public Set<Course> getCourses(){
        return this.courses;
    }

    public void setCourses(Set<Course> courses){
        this.courses = courses;
    }

    public void addCourse(Course course){
        courses.add(course);
    }

}
