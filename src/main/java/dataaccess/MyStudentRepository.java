package dataaccess;

import domain.Student;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student, Long> {

    List<Student> findAllStudentsByName(String name);
    List<Student> findAllStudentsByMajor(String major);
    List<Student> findAllStudentsByEmail(String email);
    List<Student> findAllStudentsByBirthyear(int year);

    Student addCoursesToStudent(Student student);

}
