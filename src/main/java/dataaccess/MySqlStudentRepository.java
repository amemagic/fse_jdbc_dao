package dataaccess;

import domain.Student;
import util.Assert;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlStudentRepository implements MyStudentRepository {

    private Connection con;

    public MySqlStudentRepository() throws SQLException, ClassNotFoundException {
        this.con = MysqlDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "");
    }

    private int countStudentsInDbWithId(Long id)
    {
        try {
            String countSQL = "SELECT COUNT(*) FROM `students` WHERE `id`=?";
            PreparedStatement preparedStatementCount = con.prepareStatement(countSQL);
            preparedStatementCount.setLong(1, id);
            ResultSet resultSetCount = preparedStatementCount.executeQuery();
            resultSetCount.next();
            int courseCount = resultSetCount.getInt(1);
            return courseCount;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> insert(Student entity) {
        Assert.notNull(entity);
        try
        {
            String sql = "INSERT INTO `students` (`firstname`, `lastname`, `birthdate`, `email`, `major`, `admissiondate`) VALUES (?, ?, ?, ?, ?, ?) ";
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setDate(3, entity.getBirthDate());
            preparedStatement.setString(4, entity.getEmail());
            preparedStatement.setString(5, entity.getMajor());
            preparedStatement.setDate(6, entity.getAdmissionDate());

            int affectedRows = preparedStatement.executeUpdate();

            if(affectedRows == 0)
            {
                return Optional.empty();
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if(generatedKeys.next())
            {
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }

        } catch (SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id);
        if(countStudentsInDbWithId(id)==0)
        {
            return Optional.empty();
        } else
        {
            try {
                String sql = "SELECT * FROM `students` WHERE `id` = ?";
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("birthdate"),
                        resultSet.getString("email"),
                        resultSet.getString("major"),
                        resultSet.getDate("admissiondate")
                );
                return Optional.of(student);
            } catch (SQLException sqlException)
            {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public List<Student> getAll() {
        String sql = "SELECT * FROM `students`";
        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while(resultSet.next())
            {
                studentList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("birthdate"),
                                resultSet.getString("email"),
                                resultSet.getString("major"),
                                resultSet.getDate("admissiondate")
                        )
                );
            }
            return studentList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occured!");
        }
    }

    @Override
    public Optional<Student> update(Student entity) {
        Assert.notNull(entity);

        String sql = "UPDATE `students` SET `firstname` = ?, `lastname` = ?, `birthdate` = ?, `email` = ?, `major` = ?, `admissiondate` = ? WHERE `students`.`id` = ? ";

        if(countStudentsInDbWithId(entity.getId()) == 0)
        {
            return Optional.empty();
        } else
        {
            try {
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setString(1, entity.getFirstName());
                preparedStatement.setString(2, entity.getLastName());
                preparedStatement.setDate(3, entity.getBirthDate());
                preparedStatement.setString(4, entity.getEmail());
                preparedStatement.setString(5, entity.getMajor());
                preparedStatement.setDate(6, entity.getAdmissionDate());
                preparedStatement.setLong(7, entity.getId());

                int affectedRows = preparedStatement.executeUpdate();
                if(affectedRows == 0)
                {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch(SQLException sqlException)
            {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "DELETE FROM `students` WHERE `id` = ?";
        try
        {
            if(countStudentsInDbWithId(id) != 0){
                PreparedStatement preparedStatement = con.prepareStatement(sql);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch(SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByName(String name) {
        try
        {
            String sql = "SELECT * FROM `students` WHERE LOWER(`firstname`) LIKE LOWER(?) OR LOWER (`lastname`) LIKE LOWER(?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + name + "%");
            preparedStatement.setString(2, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while(resultSet.next())
            {
                studentList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("birthdate"),
                                resultSet.getString("email"),
                                resultSet.getString("major"),
                                resultSet.getDate("admissiondate")
                        )
                );
            }
            return studentList;
        } catch (SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByMajor(String major) {
        try
        {
            String sql = "SELECT * FROM `students` WHERE `major` = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + major + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while(resultSet.next())
            {
                studentList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("birthdate"),
                                resultSet.getString("email"),
                                resultSet.getString("major"),
                                resultSet.getDate("admissiondate")
                        )
                );
            }
            return studentList;
        } catch (SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByEmail(String email) {
        try
        {
            String sql = "SELECT * FROM `students` WHERE `email` = ?";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "%" + email + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while(resultSet.next())
            {
                studentList.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getDate("birthdate"),
                                resultSet.getString("email"),
                                resultSet.getString("major"),
                                resultSet.getDate("admissiondate")
                        )
                );
            }
            return studentList;
        } catch (SQLException sqlException)
        {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByBirthyear(int year) {
        return null;
    }

    @Override
    public Student addCoursesToStudent(Student student){
        return null;
    }

}
